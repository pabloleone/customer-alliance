# README

## Set up

Checkout the repository
Add this alias to you bash profile `alias sail="[ -f sail ] && bash sail || bash vendor/bin/sail"`
Exectue this command `sail up`
To check the tests use `sail test`

NOTE: Run the tests before the seeds as these are using the main DB connection. I didn't want to overcomplicate the set up adding a new DB just for testing and sqllite (inmemory) lack of MONTH and WEEK SQL method which forced my either to write sqlite specific queries for tests or create a specific DB for testing which would be my preference.

Migrate the db using `sail artisan migrate --seed`

## Decisions

Laravel defaul code hasn't been cleaned up.
Laravel model scopes have not be tested because are always part of an integration.
