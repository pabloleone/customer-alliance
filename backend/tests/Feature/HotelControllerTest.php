<?php

namespace Tests\Feature;

use App\Models\Hotel;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class HotelControllerTest extends TestCase
{
    use RefreshDatabase;

    public function test_get_all_hotels()
    {
        Hotel::factory()->count(10)->create();

        $response = $this->getJson(route('hotels.index'));

        $response->assertStatus(200);

        $response->assertJsonCount(10, 'data');
    }
}
