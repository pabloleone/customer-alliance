<?php

namespace Tests\Feature;

use App\Models\Review;
use App\Models\Hotel;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Tests\TestCase;

class ReviewControllerTest extends TestCase
{
    use RefreshDatabase;

    public function test_get_hotel_reviews_by_day()
    {
        $hotel = Hotel::factory()->create();

        Review::factory()->count(90)->between('-29 days')->create();

        $response = $this->getJson(
            route('hotel.reviews', [
                $hotel,
                'from' => Carbon::now()->subDays('29')->toDateString(),
                'to' => Carbon::now()->toDateString()
            ])
        );

        $response->assertStatus(200);

        $response->assertJsonStructure(['data' => [
            ['review-count', 'average-score', 'date-group']]
        ]);

        $responseDecoded = collect($response->json()['data']);

        $uniqueDates = $responseDecoded->unique('date-group')->pluck('date-group');

        $dateDuplicates = $responseDecoded->pluck('date-group')->diff($uniqueDates);

        $this->assertEmpty($dateDuplicates);

        $outsideRange = $uniqueDates->filter((function($date) {
            return !($date >= Carbon::now()->subDays(30) && $date <= Carbon::now());
        }));

        $this->assertEmpty($outsideRange);
    }

    public function test_get_hotel_review_by_week()
    {
        $hotel = Hotel::factory()->create();

        Review::factory()->count(270)->between('-89 days')->create();

        $response = $this->getJson(
            route('hotel.reviews', [
                $hotel,
                'from' => Carbon::now()->subDays('89')->toDateString(),
                'to' => Carbon::now()->toDateString()
            ])
        );

        $response->assertStatus(200);

        $response->assertJsonStructure(['data' => [
            ['review-count', 'average-score', 'date-group']]
        ]);

        $responseDecoded = collect($response->json()['data']);

        $uniqueDates = $responseDecoded->unique('date-group')->pluck('date-group');

        $dateDuplicates = $responseDecoded->pluck('date-group')->diff($uniqueDates);

        $this->assertEmpty($dateDuplicates);

        $outsideRange = $uniqueDates->filter((function($date) {
            return !($date >= Carbon::now()->subDays(96)->week
                    && $date <= Carbon::now()->week);
        }));

        $this->assertEmpty($outsideRange);
    }

    public function test_get_hotel_review_by_month()
    {
        $hotel = Hotel::factory()->create();

        Review::factory()->count(360)->between('-120 days')->create();

        $response = $this->getJson(
            route('hotel.reviews', [
                $hotel,
                'from' => Carbon::now()->subDays('120')->toDateString(),
                'to' => Carbon::now()->toDateString()
            ])
        );

        $response->assertStatus(200);

        $response->assertJsonStructure(['data' => [
            ['review-count', 'average-score', 'date-group']]
        ]);

        $responseDecoded = collect($response->json()['data']);

        $uniqueDates = $responseDecoded->unique('date-group')->pluck('date-group');

        $dateDuplicates = $responseDecoded->pluck('date-group')->diff($uniqueDates);

        $this->assertEmpty($dateDuplicates);

        $outsideRange = $uniqueDates->filter((function($date) {
            return !($date >= Carbon::now()->subDays(150)->month
                    && $date <= Carbon::now()->month);
        }));

        $this->assertEmpty($outsideRange);
    }
}
