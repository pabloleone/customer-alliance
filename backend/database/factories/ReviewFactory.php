<?php

namespace Database\Factories;

use App\Models\Hotel;
use Illuminate\Database\Eloquent\Factories\Factory;

class ReviewFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'hotel_id' => Hotel::inRandomOrder()->first()->id,
            'score' => $this->faker->randomFloat(1, 0, 10),
            'comment' => $this->faker->realText(),
            'created_at' => $this->faker->dateTimeBetween('-2 years', 'now')
        ];
    }

    public function between(string $value)
    {
        return $this->state(function(array $attr) use ($value) {
            return [
                'created_at' => $this->faker->dateTimeBetween($value, 'now')
            ];
        });
    }
}
