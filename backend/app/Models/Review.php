<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Facades\DB;

class Review extends Model
{
    use HasFactory;

    public function hotel(): BelongsTo
    {
        return $this->belongsTo(Hotel::class);
    }

    public function scopeFrom($query, $from)
    {
        return $query->where('created_at', '>=', $from);
    }

    public function scopeTo($query, $to)
    {
        return $query->where('created_at', '<=', $to);
    }

    public function scopeGroupedByDateRange($query, string $range)
    {
        switch ($range) {
            case 'month':
                $query->groupBy(DB::raw('MONTH(created_at)'));
                break;

            case 'week':
                $query->groupBy(DB::raw('WEEK(created_at)'));
                break;

            default:
                $query->groupBy(DB::raw('DATE(created_at)'));
                break;
        }

        return $query;
    }

    public function scopeSelectedByDateRange($query, string $range)
    {
        $select = 'COUNT(id) AS `review-count`, ROUND(AVG(score), 1) AS `average-score`';
        switch ($range) {
            case 'month':
                $select .= ', MONTH(created_at) AS `date-group`';
                $query->select(DB::raw($select));
                break;

            case 'week':
                $select .= ', WEEK(created_at) AS `date-group`';
                $query->select(DB::raw($select));
                break;

            default:
                $select .= ', DATE(created_at) AS `date-group`';
                $query->select(DB::raw($select));
                break;
        }

        return $query;
    }
}
