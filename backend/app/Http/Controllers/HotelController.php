<?php

namespace App\Http\Controllers;

use App\Http\Resources\HotelResource;
use App\Models\Hotel;

class HotelController extends Controller
{
    public function index()
    {
        return HotelResource::collection(Hotel::all());
    }
}
