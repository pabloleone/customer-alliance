<?php

namespace App\Http\Controllers;

use App\Http\Requests\ReviewRequest;
use App\Http\Resources\ReviewResource;
use App\Models\Hotel;
use Carbon\Carbon;

class ReviewController extends Controller
{
    public function index(Hotel $hotel, ReviewRequest $request)
    {
        $range = '';

        $from = Carbon::parse($request->from);
        $to = Carbon::parse($request->to);

        $days = $from->diffInDays($to);

        if ($days > 29) {
            $range = 'week';
        }
        if ($days > 89) {
            $range = 'month';
        }

        $reviews = $hotel
            ->reviews()
            ->selectedByDateRange($range)
            ->from($from)
            ->to($to)
            ->groupedByDateRange($range)
            ->get()
            ->sortBy("date-group")
            ->map(function($review) use ($days) {
                if ($days > 29 && $days < 90) {
                    $review['date-group'] = 'Week '.$review['date-group'];
                }
                if ($days > 89) {
                    $monthName = Carbon::create()
                        ->day(1)
                        ->month((int) $review['date-group'])
                        ->monthName;
                    $review['date-group'] = $monthName;
                }
                return $review;
            });

        return ReviewResource::collection($reviews);
    }
}
