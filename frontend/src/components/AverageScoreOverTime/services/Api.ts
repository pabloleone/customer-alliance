import axios from "axios";
import { HotelService } from "./Hotel.service";

class Api {
  hotels = new HotelService(axios);
}

const api = new Api();

export default api;
