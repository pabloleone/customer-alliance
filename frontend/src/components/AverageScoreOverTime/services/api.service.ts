import { AxiosStatic } from "axios";
import { config } from "./api.config";

export abstract class ApiService {
  baseUrl: string = config.baseUrl;

  resource = "";

  constructor(public axios: AxiosStatic) {}
}
