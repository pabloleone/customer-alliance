import { Hotel } from "../types/Hotel.type";
import axios from "axios";
import { ApiService } from "./api.service";
import { ReviewsService } from "./Review.service";

export class HotelService extends ApiService {
  resource = "hotels";

  reviews = new ReviewsService(axios);

  async get(): Promise<Hotel[]> {
    const {
      data: { data },
    } = await this.axios.get(`${this.baseUrl}/${this.resource}`);
    return data as Hotel[];
  }
}
