import { Review } from "../types/Review.type";
import { ApiService } from "./api.service";

export class ReviewsService extends ApiService {
  resource = "reviews";

  async get(hotel_id: number, from: string, to: string): Promise<Review[]> {
    const {
      data: { data },
    } = await this.axios.get(
      `${this.baseUrl}/hotels/${hotel_id}/${this.resource}?from=${from}&to=${to}`
    );
    return data as Review[];
  }
}
