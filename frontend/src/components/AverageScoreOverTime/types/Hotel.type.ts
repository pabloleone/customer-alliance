export interface Hotel {
  id: number;
  name: string;
  created_at: Date;
  updated_at: Date;
}
