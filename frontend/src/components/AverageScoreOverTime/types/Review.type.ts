export interface Review {
  "review-count": number;
  "average-score": number;
  "date-group": string;
}
